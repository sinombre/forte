﻿using ForteTestApi.CoreService;
using ForteTestApi.Models.Domain;
using ForteTestApi.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ForteTestApi.Backend.Controllers
{
    public class ClientesController : ApiController
    {
        private readonly ClienteService _clienteService;

        public ClientesController()
        {
            _clienteService = new ClienteService();
        }
                
        public IEnumerable<ClienteDto> GetClientes()
        {            
            return _clienteService.GetClientes();                       
        }
        
        public HttpResponseMessage AddCliente([FromBody]Cliente cliente)
        {            
            try
            {
                if(!ModelState.IsValid)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                _clienteService.AddCliente(cliente);
                return Request.CreateResponse(HttpStatusCode.OK);
            }catch(Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
