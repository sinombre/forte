﻿using ForteTestApi.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForteTestApi.Models.Domain
{
    public class Cliente : PersonaFisica, ISoftDeleted, IAuditable
    {        
        public int ClienteId { get; set; }

        public string CorreoElectronico { get; set; }

        public string Password { get; set; }

        public string Domicilio { get; set; }

        public decimal LimiteCredito { get; set; }

        public byte EstatusClienteId { get; set; }

        public virtual EstatusCliente EstatusCliente { get; set; }

        private DateTime creadoEl;

        private bool eliminado;

        private string creadoPor;

        private DateTime? eliminadoEl;

        private string eliminadoPor;

        private DateTime? modificadoEl;

        private string modificadoPor;

        public DateTime CreadoEl
        {
            get
            {
                return creadoEl;
            }

            set
            {
                creadoEl = value;
            }
        }

        public string CreadoPor
        {
            get
            {
                return creadoPor;
            }

            set
            {
                creadoPor = value;
            }
        }
        
        public DateTime? EliminadoEl
        {
            get
            {
                return eliminadoEl;
            }

            set
            {
                eliminadoEl = value;
            }
        }

        public string EliminadoPor
        {
            get
            {
                return eliminadoPor;
            }

            set
            {
                eliminadoPor = value;
            }
        }

        public DateTime? ModificadoEl
        {
            get
            {
                return modificadoEl;
            }

            set
            {
                modificadoEl = value;
            }
        }

        public string ModificadoPor
        {
            get
            {
                return modificadoPor;
            }

            set
            {
                modificadoPor = value;
            }
        }

        public bool Eliminado
        {
            get
            {
                return eliminado;
            }

            set
            {
                eliminado = value;
            }
        }
    }
}
