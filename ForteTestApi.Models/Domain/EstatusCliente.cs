﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForteTestApi.Models.Domain
{
    public class EstatusCliente
    {
        public EstatusCliente()
        {
            Clientes = new List<Cliente>();
        }

        public byte EstatusClienteId { get; set; }

        public string Descripcion { get; set; }

        public IEnumerable<Cliente> Clientes { get; set; }
    }
}
