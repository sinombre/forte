﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForteTestApi.Models.Helpers
{
    interface IAuditable
    {
        string CreadoPor { get; set; }

        DateTime CreadoEl { get; set; }

        string ModificadoPor { get; set; }

        DateTime? ModificadoEl { get; set; }

        string EliminadoPor { get; set; }

        DateTime? EliminadoEl { get; set; }
    }
}
