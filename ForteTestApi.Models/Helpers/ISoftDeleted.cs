﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForteTestApi.Models.Helpers
{
    public interface ISoftDeleted
    {
        bool Eliminado { get; set; }
    }
}
