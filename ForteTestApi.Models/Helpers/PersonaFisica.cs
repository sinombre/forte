﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForteTestApi.Models.Helpers
{
    public class PersonaFisica
    {
        public string NombreCompleto { get; set; }

        public DateTime FechaDeNacimiento { get; set; }

        public short Edad { get { return (short)(DateTime.Now.Year - FechaDeNacimiento.Year); } }
    }
}
