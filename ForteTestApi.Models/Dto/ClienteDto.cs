﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForteTestApi.Models.Dto
{
    public class ClienteDto
    {
        public int ClienteId { get; set; }

        public string NombreCliente { get; set; }

        public string CorreoElectronico { get; set; }

        public DateTime FechaNacimiento { get; set; }

        public string EstatusCliente { get; set; }

        public decimal LimiteCredito { get; set; }
    }
}
