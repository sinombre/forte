﻿using ForteTestApi.CoreDbContext;
using ForteTestApi.Models.Domain;
using ForteTestApi.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForteTestApi.CoreService
{
    public class ClienteService
    {
        private readonly ForteDbContext _forteDbContext;

        public ClienteService()
        {
            _forteDbContext = new ForteDbContext();
        }

        public void AddCliente(Cliente cliente)
        {
            try
            {
                Cliente clienteExistente = _forteDbContext.Clientes.FirstOrDefault(x => x.CorreoElectronico.Trim() == cliente.CorreoElectronico.Trim());
                if(clienteExistente != null)
                {
                    throw new InvalidOperationException("ya existe un cliente registrado con ese correo electrónico");
                }
                else
                {
                    cliente.CreadoEl = DateTime.Now;
                    _forteDbContext.Clientes.Add(cliente);
                    _forteDbContext.SaveChanges();
                }
            }catch(Exception e)
            {
                throw new Exception("hubo un problema al guardar el cliente: "+e.Message);
            }
        }

        public IEnumerable<ClienteDto> GetClientes()
        {
            try
            {
                return _forteDbContext.Clientes.Include("EstatusCliente").Where(x => x.Eliminado == false).Select(x =>
                new ClienteDto
                {
                    ClienteId = x.ClienteId,
                    CorreoElectronico = x.CorreoElectronico,
                    EstatusCliente = x.EstatusCliente.Descripcion,
                    FechaNacimiento = x.FechaDeNacimiento,
                    LimiteCredito = x.LimiteCredito,
                    NombreCliente = x.NombreCompleto
                }).ToList();
            }
            catch (Exception e)
            {
                throw new Exception("hubo un error al intentar recuperar los clientes");
            }
        }

        public IEnumerable<EstatusCliente> GetEstado()
        {
            try
            {
                var estados = _forteDbContext.EstatusClientes.ToList();
                return estados;
            }
            catch(Exception e)
            {
                throw new Exception("hubo un error al intentar recuperar los clientes");
            }
        }
    }
}
