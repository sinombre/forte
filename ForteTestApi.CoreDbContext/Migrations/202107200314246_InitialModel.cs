﻿namespace ForteTestApi.CoreDbContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clientes",
                c => new
                    {
                        ClienteId = c.Int(nullable: false, identity: true),
                        CorreoElectronico = c.String(),
                        Password = c.String(),
                        Domicilio = c.String(),
                        LimiteCredito = c.Decimal(nullable: false, precision: 18, scale: 2),
                        EstatusClienteId = c.Byte(nullable: false),
                        CreadoEl = c.DateTime(nullable: false),
                        CreadoPor = c.String(),
                        Eliminado = c.Boolean(nullable: false),
                        EliminadoEl = c.DateTime(),
                        EliminadoPor = c.String(),
                        ModificadoEl = c.DateTime(),
                        ModificadoPor = c.String(),
                        NombreCompleto = c.String(),
                        FechaDeNacimiento = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ClienteId)
                .ForeignKey("dbo.EstatusClientes", t => t.EstatusClienteId, cascadeDelete: true)
                .Index(t => t.EstatusClienteId);
            
            CreateTable(
                "dbo.EstatusClientes",
                c => new
                    {
                        EstatusClienteId = c.Byte(nullable: false),
                        Descripcion = c.String(),
                    })
                .PrimaryKey(t => t.EstatusClienteId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Clientes", "EstatusClienteId", "dbo.EstatusClientes");
            DropIndex("dbo.Clientes", new[] { "EstatusClienteId" });
            DropTable("dbo.EstatusClientes");
            DropTable("dbo.Clientes");
        }
    }
}
