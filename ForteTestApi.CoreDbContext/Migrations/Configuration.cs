﻿namespace ForteTestApi.CoreDbContext.Migrations
{
    using Models.Domain;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ForteTestApi.CoreDbContext.ForteDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ForteTestApi.CoreDbContext.ForteDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
            context.EstatusClientes.AddOrUpdate(x => x.EstatusClienteId,
                new EstatusCliente() { EstatusClienteId = 1, Descripcion = "ACTIVO" },
                new EstatusCliente() { EstatusClienteId = 2, Descripcion = "PENDIENTE ACTIVACIÓN" },
                new EstatusCliente() { EstatusClienteId = 3, Descripcion = "INACTIVO" }
                );
        }
    }
}
