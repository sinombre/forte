﻿using ForteTestApi.Models.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForteTestApi.CoreDbContext
{
    public class ForteDbContext:DbContext
    {        
        public ForteDbContext() : base(System.Configuration.ConfigurationManager.ConnectionStrings["Forte"].ConnectionString)
        {
        }

        public DbSet<Cliente> Clientes { get; set; }

        public DbSet<EstatusCliente> EstatusClientes { get; set; }
    }
}
